﻿// <copyright file="ImageReader.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Platformer_Game
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;

    /// <summary>
    /// Képbeolvasó osztály.
    /// Hozzárendeli az adott objektumhoz az adott képet.
    /// </summary>
    public static class ImageReader
    {
        private static int size = 64;

        /// <summary>
        /// Gets or sets
        /// objektumok képének fix méretének tulajdonsága
        /// </summary>
        public static int SIZE
        {
            get
            {
                return size;
            }

            set
            {
                size = value;
            }
        }

        /// <summary>
        /// Visszaad egy BitmapImage típusú objektumot, a bemeneti útvonal alapján.
        /// </summary>
        /// <param name="path">Elérési útvonal</param>
        /// <returns>BitmapImage objektum</returns>
        public static ImageSource LoadImage(string path)
        {
            return new BitmapImage(new Uri(@"..\..\Resources\" + path, UriKind.RelativeOrAbsolute));
        }

        /// <summary>
        /// MapComponents típus alapján rendel képet objektumokhoz.
        /// </summary>
        /// <param name="type">MapComponents típus</param>
        /// <returns>BitmapImage objektum</returns>
        public static ImageSource LoadImage(MapComponents type)
        {
            string path = string.Empty;
            if (type == MapComponents.Start)
            {
                path = "empty.png";
            }
            else if (type == MapComponents.Empty)
            {
                path = "empty.png";
            }
            else if (type == MapComponents.Door)
            {
                path = "door.png";
            }
            else if (type == MapComponents.Blade)
            {
                path = "blade.png";
            }
            else if (type == MapComponents.Coin1)
            {
                path = "coin_1.png";
            }
            else if (type == MapComponents.Coin2)
            {
                path = "coin_2.png";
            }
            else if (type == MapComponents.Coin3)
            {
                path = "coin_3.png";
            }
            else if (type == MapComponents.Coin4)
            {
                path = "coin_4.png";
            }
            else if (type == MapComponents.Coin5)
            {
                path = "coin_5.png";
            }
            else if (type == MapComponents.Coin6)
            {
                path = "coin_6.png";
            }
            else if (type == MapComponents.Coin7)
            {
                path = "coin_7.png";
            }
            else if (type == MapComponents.Coin8)
            {
                path = "coin_8.png";
            }
            else if (type == MapComponents.Coin9)
            {
                path = "coin_9.png";
            }
            else if (type == MapComponents.Platform)
            {
                path = "platform0.png";
            }
            else if (type == MapComponents.Thorn)
            {
                path = "thorn1.png";
            }
            else if (type == MapComponents.PlayerRight)
            {
                path = "player_right.png";
            }
            else if (type == MapComponents.PlayerLeft)
            {
                path = "player_left.png";
            }
            else if (type == MapComponents.PlayerJumpRight)
            {
                path = "player_right_jump.png";
            }
            else if (type == MapComponents.PlayerJumpLeft)
            {
                path = "player_left_jump.png";
            }

            return LoadImage(path);
        }
    }
}
