﻿// <copyright file="GameScreen.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Platformer_Game
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Threading;

    /// <summary>
    /// GameScreen osztály, ami FrameworkElementből származik.
    /// Megjelenítésért és időzítésért felelős.
    /// </summary>
    public class GameScreen : FrameworkElement
    {
        private const int MAXMAP = 3;
        private GameModel model;
        private GameLogic logic;
        private int levelIndex;
        private DispatcherTimer t;
        private DispatcherTimer componentTimer;
        private DispatcherTimer bladeTimer;
        private int jumpCounter;
        private int jumpMoveCounter;
        private Typeface font;

        /// <summary>
        /// Initializes a new instance of the <see cref="GameScreen"/> class.
        /// </summary>
        public GameScreen()
        {
            this.Loaded += this.GameScreen_Loaded;
        }

        private void GameScreen_Loaded(object sender, RoutedEventArgs e)
        {
            this.levelIndex = 1;
            this.model = new GameModel(this.levelIndex);
            this.logic = new GameLogic(this.model);
            this.jumpCounter = 0;
            this.jumpMoveCounter = 0;
            this.font = new Typeface("Arial");
            this.InvalidateVisual();
            Window akt = Window.GetWindow(this);
            if (akt != null)
            {
                akt.KeyDown += this.GameScreen_KeyDown;
                akt.Background = new ImageBrush(new BitmapImage(new Uri(@"..\..\Resources\game_background_3.png", UriKind.RelativeOrAbsolute)));
                this.t = new DispatcherTimer();
                this.t.Interval = TimeSpan.FromMilliseconds(20);
                this.t.Tick += this.Timer_Tick;
                this.t.Start();

                this.componentTimer = new DispatcherTimer();
                this.componentTimer.Interval = TimeSpan.FromMilliseconds(50);
                this.componentTimer.Tick += this.Component_Tick;
                this.componentTimer.Start();

                this.bladeTimer = new DispatcherTimer();
                this.bladeTimer.Interval = TimeSpan.FromMilliseconds(50);
                this.bladeTimer.Tick += this.BladeTimer_Tick;
                this.bladeTimer.Start();
            }
        }

        private void BladeTimer_Tick(object sender, EventArgs e)
        {
            this.logic.MoveBlade();
        }

        private void Component_Tick(object sender, EventArgs e)
        {
            foreach (Coin p in this.model.Coins)
            {
                if (p.ImageStatus == MapComponents.Coin1)
                {
                    p.ComponentImage = ImageReader.LoadImage(MapComponents.Coin2);
                    p.ImageStatus = MapComponents.Coin2;
                }
                else if (p.ImageStatus == MapComponents.Coin2)
                {
                    p.ComponentImage = ImageReader.LoadImage(MapComponents.Coin3);
                    p.ImageStatus = MapComponents.Coin3;
                }
                else if (p.ImageStatus == MapComponents.Coin3)
                {
                    p.ComponentImage = ImageReader.LoadImage(MapComponents.Coin4);
                    p.ImageStatus = MapComponents.Coin4;
                }
                else if (p.ImageStatus == MapComponents.Coin4)
                {
                    p.ComponentImage = ImageReader.LoadImage(MapComponents.Coin5);
                    p.ImageStatus = MapComponents.Coin5;
                }
                else if (p.ImageStatus == MapComponents.Coin5)
                {
                    p.ComponentImage = ImageReader.LoadImage(MapComponents.Coin6);
                    p.ImageStatus = MapComponents.Coin6;
                }
                else if (p.ImageStatus == MapComponents.Coin6)
                {
                    p.ComponentImage = ImageReader.LoadImage(MapComponents.Coin7);
                    p.ImageStatus = MapComponents.Coin7;
                }
                else if (p.ImageStatus == MapComponents.Coin7)
                {
                    p.ComponentImage = ImageReader.LoadImage(MapComponents.Coin8);
                    p.ImageStatus = MapComponents.Coin8;
                }
                else if (p.ImageStatus == MapComponents.Coin8)
                {
                    p.ComponentImage = ImageReader.LoadImage(MapComponents.Coin9);
                    p.ImageStatus = MapComponents.Coin9;
                }
                else if (p.ImageStatus == MapComponents.Coin9)
                {
                    p.ComponentImage = ImageReader.LoadImage(MapComponents.Coin1);
                    p.ImageStatus = MapComponents.Coin1;
                }
            }
        }

#pragma warning disable SA1202 // Elements must be ordered by access
        protected override void OnRender(DrawingContext drawingContext)
#pragma warning restore SA1202 // Elements must be ordered by access
        {
            if (this.model != null)
            {
                drawingContext.DrawImage(this.model.OnePlayer.ComponentImage, new Rect(this.model.OnePlayer.CX, this.model.OnePlayer.CY, this.model.OnePlayer.SIZE, this.model.OnePlayer.SIZE));
                if (this.model.CollectedCoin == 3)
                {
                    drawingContext.DrawImage(this.model.Door.ComponentImage, new Rect(this.model.Door.CX * ImageReader.SIZE, this.model.Door.CY * ImageReader.SIZE, ImageReader.SIZE, ImageReader.SIZE));
                }

                foreach (Platform p in this.model.Platforms)
                {
                    drawingContext.DrawImage(p.ComponentImage, new Rect(p.CX * ImageReader.SIZE, p.CY * ImageReader.SIZE, ImageReader.SIZE, ImageReader.SIZE));
                }

                foreach (Coin p in this.model.Coins)
                {
                    drawingContext.DrawImage(p.ComponentImage, new Rect(p.CX * ImageReader.SIZE, p.CY * ImageReader.SIZE, ImageReader.SIZE, ImageReader.SIZE));
                }

                foreach (Empty p in this.model.Emptys)
                {
                    drawingContext.DrawImage(p.ComponentImage, new Rect(p.CX * ImageReader.SIZE, p.CY * ImageReader.SIZE, ImageReader.SIZE, ImageReader.SIZE));
                }

                foreach (Blade p in this.model.Blades)
                {
                    drawingContext.DrawImage(p.ComponentImage, new Rect(p.CX * ImageReader.SIZE, p.CY * ImageReader.SIZE, ImageReader.SIZE, ImageReader.SIZE));
                }

                foreach (Thorn p in this.model.Thorns)
                {
                    drawingContext.DrawImage(p.ComponentImage, new Rect(p.CX * ImageReader.SIZE, p.CY * ImageReader.SIZE, ImageReader.SIZE, ImageReader.SIZE));
                }

                FormattedText coinsText = new FormattedText(
                    "Coins: " + this.model.CollectedCoin.ToString(),
                    System.Globalization.CultureInfo.CurrentCulture,
                    FlowDirection.LeftToRight,
                    this.font,
                    36,
                    Brushes.White);
                FormattedText deathsText = new FormattedText(
                    "Total Deaths: " + GameModel.Death.ToString(),
                    System.Globalization.CultureInfo.CurrentCulture,
                    FlowDirection.LeftToRight,
                    this.font,
                    36,
                    Brushes.White);

                drawingContext.DrawText(coinsText, new Point(10, 10));
                drawingContext.DrawText(deathsText, new Point(10, 50));
            }
        }

        private void GameScreen_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.Right:
                    if (this.jumpMoveCounter < 3)
                    {
                        this.jumpMoveCounter++;
                        this.logic.MovePlayer(MoveDirections.Right);
                    }

                    break;
                case Key.Left:
                    if (this.jumpMoveCounter < 3)
                    {
                        this.jumpMoveCounter++;
                        this.logic.MovePlayer(MoveDirections.Left);
                    }

                    break;
                case Key.Space:
                    if (this.jumpCounter < 2)
                    {
                        this.logic.MovePlayer(MoveDirections.Jump);
                        this.jumpCounter++;
                    }

                    break;
            }

            this.InvalidateVisual();
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            this.logic.IsPlayerOnCoin();
            if (this.logic.IsPlayerDies())
            {
                this.RestartLevel();
            }

            if (this.logic.IsPlayerInAir())
            {
                this.logic.MovePlayer(MoveDirections.Fall);
            }
            else
            {
                this.jumpCounter = 0;
                this.jumpMoveCounter = 0;
            }

            if (this.model.CollectedCoin == 3 && this.logic.IsPlayerOnExit())
            {
                if (this.levelIndex < 10)
                {
                    this.levelIndex++;
                    this.NextLevel();
                }
            }

            this.InvalidateVisual();
        }

        private void RestartLevel()
        {
            this.model = new GameModel(this.levelIndex);
            this.logic = new GameLogic(this.model);
            this.InvalidateVisual();
        }

        private void NextLevel()
        {
            if (this.levelIndex <= MAXMAP)
            {
                this.model = new GameModel(this.levelIndex);
                this.logic = new GameLogic(this.model);
                this.InvalidateVisual();
            }
            else
            {
                if (MessageBox.Show("Újra kezdenéd?", "Vége a játéknak!", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                {
                    this.levelIndex = 1;
                    this.NextLevel();
                }
                else
                {
                    this.levelIndex = 1;
                    Window akt = Window.GetWindow(this);
                    akt.Close();
                }
            }
        }
    }
}
