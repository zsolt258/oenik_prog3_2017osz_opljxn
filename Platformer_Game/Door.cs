﻿// <copyright file="Door.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Platformer_Game
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Media;

    /// <summary>
    /// Ajtó komponenst definiáló osztály, amire ha rámegy a karakter, akkor mehet a következő pályára.
    /// </summary>
    public class Door : GameItem
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Door"/> class.
        /// </summary>
        /// <param name="newcx">x koordináta</param>
        /// <param name="newcy">y koordináta</param>
        public Door(double newcx, double newcy)
        {
            this.CX = newcx;
            this.CY = newcy;
            this.ComponentImage = ImageReader.LoadImage(MapComponents.Door);
        }
    }
}
