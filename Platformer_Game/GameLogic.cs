﻿// <copyright file="GameLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Platformer_Game
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Media;

    /// <summary>
    /// Mozgás irányok enum
    /// Bal, jobb, ugrás, esés
    /// </summary>
    public enum MoveDirections
    {
        Left, Right, Jump, Fall
    }

    /// <summary>
    /// Játék logikáját összefogó osztály.
    /// Felelős az irányításért és az egyéb logikai dolgok lekezeléséért.
    /// </summary>
    public class GameLogic
    {
        /// <summary>
        /// GameModel típusú objektum
        /// </summary>
        private GameModel model;

        /// <summary>
        /// Initializes a new instance of the <see cref="GameLogic"/> class.
        /// </summary>
        /// <param name="newmodel">GameModel típusú bemenet, ami inicializálja a model nevű változót.</param>
        public GameLogic(GameModel newmodel)
        {
            this.model = newmodel;
        }

        /// <summary>
        /// Ez hívódik meg, ha a felhasználó, valamelyik iránybillentyűt nyomta le, vagy csak esésben van a karakter.
        /// </summary>
        /// <param name="direction">MoveDirections enum: bal, jobb, ugrás, esés</param>
        public void MovePlayer(MoveDirections direction)
        {
            int x = Convert.ToInt32(Math.Floor(this.model.OnePlayer.CX / ImageReader.SIZE));
            int y = Convert.ToInt32(Math.Floor(this.model.OnePlayer.CY / ImageReader.SIZE));

            if (direction == MoveDirections.Right)
            {
                if (this.IsPlayerInAir() && (int)this.model.OnePlayer.CX < this.model.MapX)
                {
                    int i = 1;
                    while (i < 11 && this.model.OneMap.Map[x + 1, y + (1 / i)] != (int)MapComponents.Platform)
                    {
                        i++;
                    }

                    if (i == 11)
                    {
                        this.model.OnePlayer.CX = (int)this.model.OnePlayer.CX + (1 * ImageReader.SIZE);
                        this.model.OnePlayer.ComponentImage = ImageReader.LoadImage(MapComponents.PlayerRight);
                        this.model.OnePlayer.ImageStatus = MapComponents.PlayerRight;
                    }
                }
                else
                {
                    if ((int)this.model.OnePlayer.CX < this.model.MapX && this.model.OneMap.Map[x + 1, y] != (int)MapComponents.Platform)
                    {
                        this.model.OnePlayer.CX = (int)this.model.OnePlayer.CX + (1 * ImageReader.SIZE);
                        this.model.OnePlayer.ComponentImage = ImageReader.LoadImage(MapComponents.PlayerRight);
                        this.model.OnePlayer.ImageStatus = MapComponents.PlayerRight;
                    }
                }
            }
            else if (direction == MoveDirections.Left)
            {
                if (this.IsPlayerInAir() && (int)this.model.OnePlayer.CX > 0)
                {
                    int i = 1;
                    while (i < 11 && this.model.OneMap.Map[x - 1, y + (1 / i)] != (int)MapComponents.Platform)
                    {
                        i++;
                    }

                    if (i == 11)
                    {
                        this.model.OnePlayer.CX = this.model.OnePlayer.CX - (1 * ImageReader.SIZE);
                        this.model.OnePlayer.ComponentImage = ImageReader.LoadImage(MapComponents.PlayerLeft);
                        this.model.OnePlayer.ImageStatus = MapComponents.PlayerLeft;
                    }
                }
                else
                {
                    if ((int)this.model.OnePlayer.CX > 0 && this.model.OneMap.Map[x - 1, y] != (int)MapComponents.Platform)
                    {
                        this.model.OnePlayer.CX = this.model.OnePlayer.CX - (1 * ImageReader.SIZE);
                        this.model.OnePlayer.ComponentImage = ImageReader.LoadImage(MapComponents.PlayerLeft);
                        this.model.OnePlayer.ImageStatus = MapComponents.PlayerLeft;
                    }
                }
            }
            else if (direction == MoveDirections.Jump)
            {
                if (this.model.OnePlayer.CY > 0 && y >= 2 && this.model.OnePlayer.CY <= this.model.MapY && this.model.OneMap.Map[x, y - 2] != (int)MapComponents.Platform)
                {
                    this.model.OnePlayer.CY = this.model.OnePlayer.CY - (2 * ImageReader.SIZE);
                }
                else if (this.model.OnePlayer.CY > 0 && y >= 1 && this.model.OnePlayer.CY < this.model.MapY && this.model.OneMap.Map[x, y - 1] != (int)MapComponents.Platform)
                {
                    this.model.OnePlayer.CY = this.model.OnePlayer.CY - (1 * ImageReader.SIZE);
                }
            }
            else if (direction == MoveDirections.Fall)
            {
                if (this.model.OnePlayer.CY < this.model.MapY && this.model.OneMap.Map[x, y + 1] != (int)MapComponents.Platform)
                {
                    this.model.OnePlayer.CY = this.model.OnePlayer.CY + (1 * (ImageReader.SIZE / 8));
                }
            }
        }

        /// <summary>
        /// Végig megy az összes Blade típusú objektumon és elindítja a megfelelő irányba.
        /// Ha jobbra tart a fűrész, akkor megnézi, hogy tud-e még jobbra mozogni:
        ///     - Nincs esetleg fal, vagy platform tőle jobbra.
        ///     - Ha van, akkor haladási irányt vált.
        /// Ha balra tart a fűrész, akkor megnézi, hogy tud-e még balra mozogni:
        ///     - Nincs esetleg fal, vagy platform tőle balra.
        ///     - Ha van, akkor haladási irányt vált.
        /// </summary>
        public void MoveBlade()
        {
            foreach (Blade oneBlade in this.model.Blades)
            {
                if (oneBlade.Direction == MoveDirections.Right)
                {
                    if (oneBlade.CX < (this.model.OneMap.Map.GetLength(0) - 1))
                    {
                        int i = 1;
                        while (i < 11 && this.model.OneMap.Map[(int)oneBlade.CX + (1 / i), (int)oneBlade.CY] != (int)MapComponents.Platform)
                        {
                            i++;
                        }

                        if (i == 11)
                        {
                            oneBlade.CX = oneBlade.CX + 0.30;
                        }
                        else
                        {
                            oneBlade.Direction = MoveDirections.Left;
                        }
                    }
                    else
                    {
                        oneBlade.Direction = MoveDirections.Left;
                    }
                }
                else
                {
                    if (oneBlade.CX >= 1)
                    {
                        int i = 11;
                        while (i > 0 && this.model.OneMap.Map[(int)oneBlade.CX - (1 / i), (int)oneBlade.CY] != (int)MapComponents.Platform)
                        {
                            i--;
                        }

                        if (i == 0)
                        {
                            oneBlade.CX = oneBlade.CX - 0.30;
                        }
                        else
                        {
                            oneBlade.Direction = MoveDirections.Right;
                        }
                    }
                    else
                    {
                        oneBlade.Direction = MoveDirections.Right;
                    }
                }
            }
        }

        /// <summary>
        /// Vizsgálja, hogy a játékos érmén van-e.
        /// Ha igen, akkor feltudja szedni a pályáról.
        /// </summary>
        public void IsPlayerOnCoin()
        {
            if (this.model.OneMap.Map[(int)(this.model.OnePlayer.CX / ImageReader.SIZE), (int)(this.model.OnePlayer.CY / ImageReader.SIZE)] == (int)MapComponents.Coin1)
            {
                this.model.CollectedCoin++;
                this.model.OneMap.Map[(int)this.model.OnePlayer.CX / ImageReader.SIZE, (int)this.model.OnePlayer.CY / ImageReader.SIZE] = (int)MapComponents.Empty;
                int i = 0;
                while (i < this.model.Coins.Count && (this.model.Coins[i].CX != this.model.OnePlayer.CX / ImageReader.SIZE) && (this.model.Coins[i].CY != this.model.OnePlayer.CY / ImageReader.SIZE))
                {
                    i++;
                }

                this.model.Coins.Remove(this.model.Coins[i]);
            }
        }

        /// <summary>
        /// Vizsgálja, hogy a játékos a kijáraton áll-e.
        /// </summary>
        /// <returns>Ha igaz, akkor betöltődik a következő pálya</returns>
        public bool IsPlayerOnExit()
        {
            if (this.model.OneMap.Map[(int)(this.model.OnePlayer.CX / ImageReader.SIZE), (int)(this.model.OnePlayer.CY / ImageReader.SIZE)] == (int)MapComponents.Door)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Vizsgálja, hogy a játékos levegőben van-e.
        /// Akkor van levegőben, ha az alatt lévő mező üres.
        /// </summary>
        /// <returns>Ha igaz, akkor a játékos, akkor meghívódik egy olyan metódus, ami beállítja a játékosnak a mozgási irányát esésre, és annak megfelelően hívódik meg a MovePlayer metódus</returns>
        public bool IsPlayerInAir()
        {
            if ((int)this.model.OnePlayer.CY < this.model.MapY && this.model.OneMap.Map[(int)(this.model.OnePlayer.CX / ImageReader.SIZE), ((int)this.model.OnePlayer.CY / ImageReader.SIZE) + 1] != (int)MapComponents.Platform)
            {
                if (this.model.OnePlayer.ImageStatus == MapComponents.PlayerRight)
                {
                    this.model.OnePlayer.ComponentImage = ImageReader.LoadImage(MapComponents.PlayerJumpRight);
                    this.model.OnePlayer.ImageStatus = MapComponents.PlayerJumpRight;
                }
                else if (this.model.OnePlayer.ImageStatus == MapComponents.PlayerLeft)
                {
                    this.model.OnePlayer.ComponentImage = ImageReader.LoadImage(MapComponents.PlayerJumpLeft);
                    this.model.OnePlayer.ImageStatus = MapComponents.PlayerJumpLeft;
                }

                return true;
            }
            else
            {
                if (this.model.OnePlayer.ImageStatus == MapComponents.PlayerJumpRight)
                {
                    this.model.OnePlayer.ComponentImage = ImageReader.LoadImage(MapComponents.PlayerRight);
                    this.model.OnePlayer.ImageStatus = MapComponents.PlayerRight;
                }
                else if (this.model.OnePlayer.ImageStatus == MapComponents.PlayerJumpLeft)
                {
                    this.model.OnePlayer.ComponentImage = ImageReader.LoadImage(MapComponents.PlayerLeft);
                    this.model.OnePlayer.ImageStatus = MapComponents.PlayerLeft;
                }

                return false;
            }
        }

        /// <summary>
        /// Vizsgálja, hogy meghalt-e a játékos.
        /// Két akadálytól halhat meg:
        ///     - Fűrész
        ///     - Tüske
        /// </summary>
        /// <returns>Ha igaz, akkor a játékos meghalt és kezdheti előről a gyűjtögetést a pálya elejéről.</returns>
        public bool IsPlayerDies()
        {
            if ((int)this.model.OnePlayer.CY <= this.model.MapY
                && this.model.OneMap.Map[(int)(this.model.OnePlayer.CX / ImageReader.SIZE), (int)(this.model.OnePlayer.CY / ImageReader.SIZE)] == (int)MapComponents.Thorn)
            {
                GameModel.Death++;
                return true;
            }
            else
            {
                foreach (Blade oneBlade in this.model.Blades)
                {
                    if ((int)oneBlade.CX == (int)(this.model.OnePlayer.CX / ImageReader.SIZE) && (int)oneBlade.CY == (int)(this.model.OnePlayer.CY / ImageReader.SIZE))
                    {
                        GameModel.Death++;
                        this.model.OnePlayer.CX = this.model.PlayerStartPosition.X;
                        this.model.OnePlayer.CY = this.model.PlayerStartPosition.Y;
                        return true;
                    }
                }
            }

            return false;
        }
    }
}
