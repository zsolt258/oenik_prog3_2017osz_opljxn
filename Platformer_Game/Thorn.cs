﻿// <copyright file="Thorn.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Platformer_Game
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Media;

    /// <summary>
    /// Tüske osztály
    /// </summary>
    public class Thorn : GameItem
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Thorn"/> class.
        /// </summary>
        /// <param name="newcx">x koordináta</param>
        /// <param name="newcy">y koordináta</param>
        public Thorn(double newcx, double newcy)
        {
            this.CX = newcx;
            this.CY = newcy;
            this.ComponentImage = ImageReader.LoadImage(MapComponents.Thorn);
        }
    }
}
