﻿// <copyright file="GameModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Platformer_Game
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// GameModel osztály, összefogja azokat az objektumokat, amik a játékhoz kellenek.
    /// </summary>
    public class GameModel
    {
        /// <summary>
        /// Halálokat számoló változó.
        /// </summary>
        private static int death = 0;

        /// <summary>
        /// Játékos kezdőpozíciója
        /// </summary>
        private Point playerStartPosition;

        /// <summary>
        /// Initializes a new instance of the <see cref="GameModel"/> class.
        /// </summary>
        /// <param name="mapIndex">Pálya sorszáma</param>
        public GameModel(int mapIndex)
        {
            this.CollectedCoin = 0;
            this.OneMap = new MapModel("..\\..\\Resources\\map" + mapIndex + ".level", out this.playerStartPosition);
            this.OnePlayer = new Player(this.playerStartPosition.X, this.playerStartPosition.Y, ImageReader.SIZE);

            this.MapX = (this.OneMap.Map.GetLength(0) - 1) * ImageReader.SIZE;
            this.MapY = (this.OneMap.Map.GetLength(1) - 1) * ImageReader.SIZE;

            this.Platforms = new List<Platform>();
            this.Coins = new List<Coin>();
            this.Emptys = new List<Empty>();
            this.Blades = new List<Blade>();
            this.Thorns = new List<Thorn>();

            for (int y = 0; y < this.OneMap.Map.GetLength(1); y++)
            {
                for (int x = 0; x < this.OneMap.Map.GetLength(0); x++)
                {
                    if (this.OneMap.Map[x, y] == (int)MapComponents.Empty || this.OneMap.Map[x, y] == (int)MapComponents.Start)
                    {
                        this.Emptys.Add(new Empty(x, y));
                    }
                    else if (this.OneMap.Map[x, y] == (int)MapComponents.Door)
                    {
                        this.Door = new Door(x, y);
                    }
                    else if (this.OneMap.Map[x, y] == (int)MapComponents.Blade)
                    {
                        this.Blades.Add(new Blade(x, y));
                    }
                    else if (this.OneMap.Map[x, y] == (int)MapComponents.Thorn)
                    {
                        this.Thorns.Add(new Thorn(x, y));
                    }
                    else if (this.OneMap.Map[x, y] == (int)MapComponents.Platform)
                    {
                        this.Platforms.Add(new Platform(x, y));
                    }
                    else if (this.OneMap.Map[x, y] == (int)MapComponents.Coin1)
                    {
                        this.Coins.Add(new Coin(x, y));
                    }
                }
            }
        }

        /// <summary>
        /// Gets or sets
        /// Halálok számának tulajdonsága.
        /// </summary>
        public static int Death
        {
            get
            {
                return death;
            }

            set
            {
                death = value;
            }
        }

        /// <summary>
        ///  Gets or sets
        ///  Összegyűjtött coinok száma.
        ///  3db után megjelenik az ajtó.
        /// </summary>
        public int CollectedCoin { get; set; }

        /// <summary>
        /// Gets map
        /// </summary>
        public MapModel OneMap { get; private set; }

        /// <summary>
        /// Gets
        /// Játékos tulajdonság
        /// </summary>
        public Player OnePlayer { get; private set; }

        /// <summary>
        /// Gets
        /// Coin lista tulajdonság
        /// </summary>
        public List<Coin> Coins { get; private set; }

        /// <summary>
        /// Gets
        /// Platformok listájának tulajdonsága
        /// </summary>
        public List<Platform> Platforms { get; private set; }

        /// <summary>
        /// Gets
        /// Ajtó objektumnak a tulajdonsága.
        /// </summary>
        public Door Door { get; private set; }

        /// <summary>
        /// Gets
        /// Üres komponenseknek a listája
        /// </summary>
        public List<Empty> Emptys { get; private set; }

        /// <summary>
        /// Gets
        /// Fűrészek listájának tulajdonsága
        /// </summary>
        public List<Blade> Blades { get; private set; }

        /// <summary>
        /// Gets
        /// Tüskék tulajdonsága
        /// </summary>
        public List<Thorn> Thorns { get; private set; }

        /// <summary>
        /// Gets
        /// pálya szélessége
        /// </summary>
        public int MapX { get; private set; }

        /// <summary>
        /// Gets
        /// pálya magassága
        /// </summary>
        public int MapY { get; private set; }

        /// <summary>
        /// Gets or sets
        /// Játékos pozíciójának tulajdonsága
        /// </summary>
        public Point PlayerStartPosition
        {
            get
            {
                return this.playerStartPosition;
            }

            set
            {
                this.playerStartPosition = value;
            }
        }
    }
}
