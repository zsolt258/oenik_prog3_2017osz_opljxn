﻿// <copyright file="ViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Platformer_Game
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// ViewModel, amely az adatkötéshez kell.
    /// </summary>
    public class ViewModel
    {
        private string[] resolution;
        private string selectedResolution;

        /// <summary>
        /// Initializes a new instance of the <see cref="ViewModel"/> class.
        /// </summary>
        public ViewModel()
        {
            this.resolution = new string[] { "400*500", "600*800", "800*1000", "1000*1300" };
        }

        /// <summary>
        /// Gets or sets
        /// Felbontásokat tartalmazó string tömb.
        /// </summary>
        public string[] Resolution
        {
            get
            {
                return this.resolution;
            }

            set
            {
                this.resolution = value;
            }
        }

        /// <summary>
        /// Gets or sets
        /// A listboxban kiválasztott felbontást tartalmazza adatkötéssel
        /// </summary>
        public string SelectedResolution
        {
            get
            {
                return this.selectedResolution;
            }

            set
            {
                this.selectedResolution = value;
            }
        }
    }
}
