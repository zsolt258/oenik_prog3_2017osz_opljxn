﻿// <copyright file="GameWindow.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Platformer_Game
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Shapes;

    /// <summary>
    /// Interaction logic for GameWindow.xaml
    /// </summary>
    public partial class GameWindow : Window
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GameWindow"/> class.
        /// </summary>
        public GameWindow()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="GameWindow"/> class.
        /// </summary>
        /// <param name="selectedResolution">Kiválaszott felbontás</param>
        public GameWindow(string selectedResolution)
            : this()
        {
            if (selectedResolution.Equals("400*500"))
            {
                this.Height = 422;
                this.Width = 527;
                ImageReader.SIZE = 32;
            }
            else if (selectedResolution.Equals("600*800"))
            {
                this.Height = 615;
                this.Width = 780;
                ImageReader.SIZE = 48;
            }
            else if (selectedResolution.Equals("800*1000"))
            {
                this.Height = 808;
                this.Width = 1041;
                ImageReader.SIZE = 64;
            }
            else
            {
                this.Height = 995;
                this.Width = 1298;
                ImageReader.SIZE = 80;
            }
        }
    }
}
