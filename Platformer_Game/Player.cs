﻿// <copyright file="Player.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Platformer_Game
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Media;

    /// <summary>
    /// Játékost megvalósító osztály.
    /// </summary>
    public class Player : GameItem
    {
        private int size;

        /// <summary>
        /// Initializes a new instance of the <see cref="Player"/> class.
        /// </summary>
        /// <param name="newcx">x koordináta</param>
        /// <param name="newcy">y koordináta</param>
        /// <param name="size">Játékos mérete</param>
        public Player(double newcx, double newcy, int size)
        {
            this.CX = newcx * size;
            this.CY = newcy * size;
            this.size = size;
            this.ComponentImage = ImageReader.LoadImage(MapComponents.PlayerRight);
            this.ImageStatus = MapComponents.PlayerRight;
        }

        /// <summary>
        /// Gets
        /// Játékos méretének tulajdonsága.
        /// </summary>
        public int SIZE
        {
            get
            {
                return this.size;
            }
        }
    }
}
