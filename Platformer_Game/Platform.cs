﻿// <copyright file="Platform.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Platformer_Game
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Media;

    /// <summary>
    /// Platform komponenst definiáló osztály.
    /// </summary>
    public class Platform : GameItem
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Platform"/> class.
        /// </summary>
        /// <param name="newcx">x koordináta</param>
        /// <param name="newcy">y koordináta</param>
        public Platform(double newcx, double newcy)
        {
            this.CX = newcx;
            this.CY = newcy;
            this.ComponentImage = ImageReader.LoadImage(MapComponents.Platform);
        }
    }
}
