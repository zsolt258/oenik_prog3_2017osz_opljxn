﻿// <copyright file="Coin.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Platformer_Game
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Media;

    /// <summary>
    /// Érme komponenst definiáló osztály, amit a karakternek össze kell gyűjteni.
    /// </summary>
    public class Coin : GameItem
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Coin"/> class.
        /// </summary>
        /// <param name="newcx">x koordináta</param>
        /// <param name="newcy">y koordináta</param>
        public Coin(double newcx, double newcy)
        {
            this.CX = newcx;
            this.CY = newcy;
            this.ComponentImage = ImageReader.LoadImage(MapComponents.Coin1);
            this.ImageStatus = MapComponents.Coin1;
        }
    }
}
