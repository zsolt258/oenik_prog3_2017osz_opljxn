﻿// <copyright file="MapModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Platformer_Game
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Pálya komponenseket definiál.
    /// </summary>
    public enum MapComponents
    {
        Start, Empty, Platform, Coin1, Coin2, Coin3, Coin4, Coin5, Coin6, Coin7, Coin8, Coin9, Blade, Thorn, Door, PlayerRight, PlayerLeft, PlayerJumpRight, PlayerJumpLeft
    }

    /// <summary>
    /// MapModel osztály, ami a pályát dolgozza fel egy külső fájlból.
    /// </summary>
    public class MapModel
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MapModel"/> class.
        /// </summary>
        /// <param name="mapLevel">pályának a neve.</param>
        /// <param name="playerStartPosition">játékos kezdőpozíciója a pálya alapján.</param>
        public MapModel(string mapLevel, out Point playerStartPosition)
        {
            this.Map = this.MapLoader(mapLevel, out playerStartPosition);
        }

        /// <summary>
        /// Gets or sets
        /// Map tulajdonság, egész számokat tartalmazó két dimenziós tömb.
        /// </summary>
        public int[,] Map { get; set; }

        private int[,] MapLoader(string mapLevel, out Point playerStartPosition)
        {
            string[] readAllLineFromFile = File.ReadAllLines(mapLevel, Encoding.UTF8);
            int[,] map = new int[readAllLineFromFile[0].Length, readAllLineFromFile.Length];
            playerStartPosition = new Point(0, 0);

            for (int y = 0; y < map.GetLength(1); y++)
            {
                for (int x = 0; x < map.GetLength(0); x++)
                {
                    if (readAllLineFromFile[y][x] == ' ')
                    {
                        map[x, y] = (int)MapComponents.Empty;
                    }
                    else if (readAllLineFromFile[y][x] == 'S')
                    {
                        map[x, y] = (int)MapComponents.Start;
                        playerStartPosition = new Point(x, y);
                    }
                    else if (readAllLineFromFile[y][x] == 'D')
                    {
                        map[x, y] = (int)MapComponents.Door;
                    }
                    else if (readAllLineFromFile[y][x] == 'B')
                    {
                        map[x, y] = (int)MapComponents.Blade;
                    }
                    else if (readAllLineFromFile[y][x] == 'T')
                    {
                        map[x, y] = (int)MapComponents.Thorn;
                    }
                    else if (readAllLineFromFile[y][x] == 'P')
                    {
                        map[x, y] = (int)MapComponents.Platform;
                    }
                    else if (readAllLineFromFile[y][x] == 'C')
                    {
                        map[x, y] = (int)MapComponents.Coin1;
                    }
                }
            }

            return map;
        }
    }
}
