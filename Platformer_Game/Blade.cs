﻿// <copyright file="Blade.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Platformer_Game
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Media;

    /// <summary>
    /// Fűrész komponenst megvalósító osztály, ami a karakterre nézve akadály.
    /// </summary>
    public class Blade : GameItem
    {
        /// <summary>
        /// Haladási iránya a fűszérnek: jobbra vagy balra mehet.
        /// </summary>
        private MoveDirections direction;

        /// <summary>
        /// Initializes a new instance of the <see cref="Blade"/> class.
        /// </summary>
        /// <param name="newcx">x koordináta</param>
        /// <param name="newcy">y koordináta</param>
        public Blade(double newcx, double newcy)
        {
            this.CX = newcx;
            this.CY = newcy;
            this.direction = MoveDirections.Right;
            this.ComponentImage = ImageReader.LoadImage(MapComponents.Blade);
        }

        /// <summary>
        /// Gets or sets
        /// A fűrész haladási irányának tulajdonsága
        /// </summary>
        public MoveDirections Direction
        {
            get
            {
                return this.direction;
            }

            set
            {
                this.direction = value;
            }
        }
    }
}
