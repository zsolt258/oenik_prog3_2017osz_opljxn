﻿// <copyright file="GameItem.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Platformer_Game
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Media;

    /// <summary>
    /// GameItem osztály, ebből szármozik a többi komponens osztálya.
    /// </summary>
    public abstract class GameItem
    {
        /// <summary>
        /// Gets or sets
        /// GameItem x koordinátájánank publikus tulajdonsága
        /// </summary>
        public double CX { get; set; }

        /// <summary>
        /// Gets or sets
        /// GameItem y koordinátájának publikus tulajdonsága
        /// </summary>
        public double CY { get; set; }

        /// <summary>
        /// Gets or sets
        /// Komponens képét tárolja egy ImageSource objektumba.
        /// </summary>
        public ImageSource ComponentImage { get; set; }

        /// <summary>
        /// Gets or sets
        /// Komponens kép állapota, ami a MapComponents enum értékeit veheti fel.
        /// Ez alapján kerül kirajzolásra a komponens képe.
        /// </summary>
        public MapComponents ImageStatus { get; set; }
    }
}
