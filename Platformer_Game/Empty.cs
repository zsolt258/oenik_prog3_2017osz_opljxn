﻿// <copyright file="Empty.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Platformer_Game
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Media;

    /// <summary>
    /// Üres komponenst definiáló osztály, ezen mehet át a karakter.
    /// </summary>
    public class Empty : GameItem
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Empty"/> class.
        /// </summary>
        /// <param name="newcx">x koordináta</param>
        /// <param name="newcy">y koordináta</param>
        public Empty(double newcx, double newcy)
        {
            this.CX = newcx;
            this.CY = newcy;
            this.ComponentImage = ImageReader.LoadImage(MapComponents.Empty);
        }
    }
}
