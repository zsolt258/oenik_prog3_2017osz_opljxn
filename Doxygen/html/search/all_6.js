var searchData=
[
  ['imagestatus',['ImageStatus',['../class_platformer___game_1_1_game_item.html#a50d1875512308ab321863c49864f6ab7',1,'Platformer_Game::GameItem']]],
  ['initializecomponent',['InitializeComponent',['../class_platformer___game_1_1_app.html#a7174f27b8c2074865702503b53c848ec',1,'Platformer_Game.App.InitializeComponent()'],['../class_platformer___game_1_1_app.html#a7174f27b8c2074865702503b53c848ec',1,'Platformer_Game.App.InitializeComponent()'],['../class_platformer___game_1_1_game_window.html#ad14643d124bc079a86e878be711d5970',1,'Platformer_Game.GameWindow.InitializeComponent()'],['../class_platformer___game_1_1_game_window.html#ad14643d124bc079a86e878be711d5970',1,'Platformer_Game.GameWindow.InitializeComponent()'],['../class_platformer___game_1_1_main_window.html#a39b1155abc25b1951167266d9e75b168',1,'Platformer_Game.MainWindow.InitializeComponent()'],['../class_platformer___game_1_1_main_window.html#a39b1155abc25b1951167266d9e75b168',1,'Platformer_Game.MainWindow.InitializeComponent()']]],
  ['isplayerdies',['IsPlayerDies',['../class_platformer___game_1_1_game_logic.html#afa31654c534e1063670ed56d538061f1',1,'Platformer_Game::GameLogic']]],
  ['isplayerinair',['IsPlayerInAir',['../class_platformer___game_1_1_game_logic.html#a497923aead5c99d7b622f52f546ef48c',1,'Platformer_Game::GameLogic']]],
  ['isplayeroncoin',['IsPlayerOnCoin',['../class_platformer___game_1_1_game_logic.html#a93bea42d44730940fcb3468f8b6b3dc9',1,'Platformer_Game::GameLogic']]],
  ['isplayeronexit',['IsPlayerOnExit',['../class_platformer___game_1_1_game_logic.html#a50e0125ec6a3fd9725764d9c43763ab2',1,'Platformer_Game::GameLogic']]]
];
