var searchData=
[
  ['gamelogic',['GameLogic',['../class_platformer___game_1_1_game_logic.html#ae084bd31151eeb22cbdb04198a4db38a',1,'Platformer_Game::GameLogic']]],
  ['gamemodel',['GameModel',['../class_platformer___game_1_1_game_model.html#aa862336df08ff3b53bdd418d7642d784',1,'Platformer_Game::GameModel']]],
  ['gamescreen',['GameScreen',['../class_platformer___game_1_1_game_screen.html#a947cb54fcc321d7a50c63c2ede35def1',1,'Platformer_Game::GameScreen']]],
  ['gamewindow',['GameWindow',['../class_platformer___game_1_1_game_window.html#ae94c310f77ec9a4ef7212aa469496560',1,'Platformer_Game.GameWindow.GameWindow()'],['../class_platformer___game_1_1_game_window.html#a7bceb9f3331cb24d81a9fe9458dc0f2b',1,'Platformer_Game.GameWindow.GameWindow(string selectedResolution)']]],
  ['getpropertyvalue',['GetPropertyValue',['../class_xaml_generated_namespace_1_1_generated_internal_type_helper.html#afdc9fe15b56607d02082908d934480c6',1,'XamlGeneratedNamespace.GeneratedInternalTypeHelper.GetPropertyValue(System.Reflection.PropertyInfo propertyInfo, object target, System.Globalization.CultureInfo culture)'],['../class_xaml_generated_namespace_1_1_generated_internal_type_helper.html#afdc9fe15b56607d02082908d934480c6',1,'XamlGeneratedNamespace.GeneratedInternalTypeHelper.GetPropertyValue(System.Reflection.PropertyInfo propertyInfo, object target, System.Globalization.CultureInfo culture)']]]
];
